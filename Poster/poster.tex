\include{header}

\title{\Huge {Theory of rotational columnar \\ structures of soft spheres}}
\author{\underline{J.~Winkelmann}, A. Mughal$^{(*)}$, D.B. Williams, D. Weaire and S.~Hutzler}
\subtitle{
School of Physics, Trinity College Dublin, The University of Dublin, Ireland \\
(*) Department of Mathematics, Aberystwyth University, Aberystwyth, Wales
}
\date{}

\titlegraphic{%
  \includegraphics[clip=true, trim= 40 0 30 10,width=\linewidth-2cm]{Abbildungen/trinity-stacked.jpg}
}

\institute{%
 \includegraphics[width=\linewidth+4cm, clip=true, trim=0 0 111 35]{Abbildungen/foamarms.png}%
}

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\centering

\begin{block}[sharp corners=all, rounded corners=north]{Columnar structures in a rotating fluid: A novel self-assembly method! [credits to Lee \textit{et al.}]}
\begin{columns}[T]
\centering
\column{0.25\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/RotColumn/rotcolumn.pdf}

\column{0.5\textwidth}
\begin{columns}
\column{0.5\textwidth}
\centering
{\LARGE\textbf{Experiment:}}
\begin{itemize}
\item
Polymeric beads of mass $M$ and diameter $d$ suspended in a fluid of higher density
\item
System is rotated with velocity $\omega$ inside a lathe
\end{itemize}

\column{0.5\textwidth}
\centering
{\LARGE\textbf{Simulation:}}
\begin{itemize}
\item
\textit{Intensive} molecular dynamics simulation to reproduce experiment
\item
Bead interaction: \enquote{partially latching spring model}
\end{itemize}
\end{columns}

\vspace{0.8cm}

\begin{itemize}[leftmargin=0.2\textwidth]
%\setlength{\itemindent}{0.2\textwidth}
\item
Centripetal force $\vec{F}_c$ moves beads to the centre
\item
Rot.{} energy depends on radial displacement $R$
\end{itemize}
\begin{equation}
\LARGE{E_{\text{rot}}(R) = \frac{1}{2} m \omega^2 R^2}
\end{equation}
\begin{itemize}[leftmargin=0.2\textwidth]
\item
\textcolor{blue}{Homogeneous structures} and \alert{binary mixtures} \\ observed for varying $\omega$ and number density $\rho$
\end{itemize}

\column{0.25\textwidth}
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.77\textwidth]{Abbildungen/ExpPacking.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (1.16, 0.9) {\Large{$(2, 2, 0)$}};
\node at (1.16, 0.64) {\Large{$(3, 2, 1)$}};
\node at (1.16, 0.36) {\Large{$(3, 3, 0)$}};
\node at (1.16, 0.1) {\Large{$(4, 2, 2)$}};
\end{scope}
\end{tikzpicture}

\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.593\textwidth]{Abbildungen/mixed-structures.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\node at (1.35, 0.78) {\Large{$(3, 2, 1) / (4, 2, 2)$}};
\node at (1.35, 0.27) {\Large{$(3, 2, 1) / (4, 2, 2)$}};
\end{scope}
\end{tikzpicture}
\captionof{figure}{Homogeneous columnar packings and binary mixtures. [credits to Lee \textit{et al.}]}
\end{columns}

\vspace{0.4cm}
\flushleft
\large{
\href{http://onlinelibrary.wiley.com/doi/10.1002/adma.201704274/abstract}{
T. Lee, K. Gizynski, B.A. Grzybowski, \emph{Non-equilibrium Self-Assembly of Monocomponent and Multicomponent Tubular Structures in Rotating Fluids.} Adv. Mater. \textbf{29}, 1704274, (2017).}
}
\end{block}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{tcolorbox}[
arc=5mm,
colback=white,
colframe=tugreen,
%text width=0.99\textwidth,
halign=center,
boxrule=3pt,
width=0.9926\textwidth,
]
\centering
\Huge{
We present \textit{analytic} energy calculations for self-assembled columnar packings, based on a generic soft sphere model, from which we obtain a comprehensive phase diagram with \textit{peritectoid} transition points.
}
\end{tcolorbox}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{block}[arc=2mm]{Ordered homogeneous packings}
\begin{columns}[b]
%1
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=10 30 10 30]{Abbildungen/211.png}
\captionof{figure}{$(2, 1, 1)$}

%2
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=40 30 40 30]{Abbildungen/220.png}
\captionof{figure}{$(2, 2, 0)$}

%3
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=25 30 25 30]{Abbildungen/321.png}
\captionof{figure}{$(3, 2, 1)$}

%4
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=20 50 20 50]{Abbildungen/330.png}
\captionof{figure}{$(3, 3, 0)$}

%5
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=28 34 28 31]{Abbildungen/422.png}
\captionof{figure}{$(4, 2, 2)$}

%6
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=35 35 35 33]{Abbildungen/431.png}
\captionof{figure}{$(4, 3, 1)$}

%7
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=40 42 40 42]{Abbildungen/440.png}
\captionof{figure}{$(4, 4, 0)$}

%8
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=20 30 20 20]{Abbildungen/532.png}
\captionof{figure}{$(5, 3, 2)$}

%9
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=33 37 33 37]{Abbildungen/541.png}
\captionof{figure}{$(5, 4, 1)$}

%10
\column{0.1\textwidth}
\centering
\includegraphics[height=0.11\textheight, clip=true, trim=20 45 20 43]{Abbildungen/550.png}
\captionof{figure}{$(5, 5, 0)$}


\end{columns}
\end{block}
\begin{columns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\column{0.494\textwidth}
\begin{block}[arc=2mm]{\textit{Analytic} soft sphere energy calculations}
\centering
\textbf{Previously: \textit{Intensive} computer simulations to find structures}
\vspace{-0.4cm}
\begin{columns}[T]
\column{0.5\textwidth}
\vspace{-0.05cm}
\begin{itemize}
\item
Energy for homogeneous structures
\begin{equation}
\frac{E^S(R)}{M\omega^2 d^2} = \underbrace{\frac{1}{2} \frac{R^2}{d^2}}_{\text{rot. Energy}} + \underbrace{\frac{1}{2} \frac{k}{M\omega^2} \avg{\left( \frac{\delta_{ij}}{d}\right)^2}}_{\text{soft sphere energy}}
\end{equation}
\begin{itemize}
\large
\item[--]
soft sphere energy depends on overlap $\delta_{ij}$
\item[--]
$\avg{\left(\delta_{ij}/d\right)^2}$ calculated for each structure
\end{itemize}
\item
Energy minimisation wrt to only $R$
\end{itemize}
\vspace{0.69cm}
\begin{itemize}
\item[$\bullet$]
\textcolor{blue}{Homogeneous structures:} Minimal energies given by coloured curves within coloured regions
\item[$\bullet$]
\alert{Binary mixtures:} Energies given by common tangent within white regions (Maxwell~construction)
\end{itemize}
\column{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/Energies.pdf}
\vspace{-0.2cm}
\captionof{figure}{Minimal energies of homogeneous (coloured) and mixed (black) structures.}
\end{columns}
\end{block}

\column{0.494\textwidth}
\begin{block}[arc=2mm]{The phase diagram}
\centering
\textbf{Which structures occur under what conditions?}
\vspace{-0.05cm}
\begin{columns}[T]
\column{0.5\textwidth}
\centering
\includegraphics[width=\textwidth]{Abbildungen/PhaseDiagramLow.pdf}
\vspace{-0.7cm}
\begin{itemize}
\item
\textcolor{blue}{Homogeneous structures}: coloured area
\item
\alert{Binary mixtures}: white area in-between
\end{itemize}
\vspace{0.04cm}
\column{0.5\textwidth}

\begin{itemize}
\item
$(5, 5, 0)$ last structure without inner spheres $\Rightarrow$ no right hand boundary
\item
Chiral structure can store energy by twisting, but achiral structure cannot
\item[$\Rightarrow$]
Achiral $(3, 3, 0)$ and $(4, 4, 0)$ structures vanish in peritectoid points
\end{itemize}
\vspace{0.1cm}
\begin{itemize}
\item
\textbf{Peritectoid point}:
\begin{itemize}
\large
\item[--]
Homogeneous structure vanish together with adjacent binary mixtures
\item[--]
New binary mixture of adjacent homogeneous structure arises
\end{itemize}
\end{itemize}
\vspace{0.68cm}
\flushleft
\large{
\href{http://onlinelibrary.wiley.com/doi/10.1002/adma.201704274/abstract}{
J. Winkelmann, A. Mughal, D.B. Williams, D. Weaire, S. Hutzler \emph{Theory of rotational columnar structures of soft spheres.} Phys Rev Letters \textit{submitted}, (2018).}
}
\end{columns}

\end{block}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{columns}

\begin{block}[sharp corners=all, rounded corners=south]{Finite-sized soft sphere simulations}

\begin{columns}[b]
\column{0.25\textwidth}
\centering
\includegraphics[width=0.94\textwidth]{Abbildungen/EnergiesSim.pdf}
\captionof{figure}{
Energies of finite-size simulation and analytic calculations (blue \& purple curves).
}
\column{0.42\textwidth}
\centering
\textbf{The \enquote{full} soft sphere energy model:}
\begin{equation}
\frac{E(\{\vec{r}_i\}, \alpha)}{M\omega^2 d^2} = \frac{1}{2} \sum_i^N \left( \frac{R_i^2}{d^2}\right)^2 + \frac{1}{2} \frac{k}{M \omega^2} \sum_{ij}^N \frac{|\vec{r}_i - \vec{r}_j|^2}{d^2}
\end{equation}
\begin{itemize}
\item
Simulation cell length $L$ containing $N$ spheres $\Rightarrow$ $\rho = N / L$
\item
periodic boundaries with translated and twisted image spheres 
\item
Energy minimisation wrt to \emph{all} sphere positions $\vec{r}_i$ and twist~angle~$\alpha$
\end{itemize}
\vspace{0.6cm}

\begin{tcolorbox}[
arc=5mm,
colback=white,
colframe=tugreen,
%text width=0.99\textwidth,
halign=center,
boxrule=1.5pt,
width=0.9926\textwidth,
]
\centering
\textbf{Energy comparison:}
\begin{itemize}
\item
Deviations only for binary mixtures due to \textbf{interface energies}
\end{itemize}
\end{tcolorbox}
\vspace{0.1cm}

\begin{tcolorbox}[
arc=5mm,
colback=tulight,
colframe=tugreen,
%text width=0.99\textwidth,
halign=center,
boxrule=3pt,
width=0.9926\textwidth,
]
\centering
\textbf{Phase diagram comparison:}
\begin{itemize}
\item
Phase diagram from finite-size simulations agrees well with analytic~calculations in semi-quantitive sense
\item
The homogeneous \textcolor{irishgreen}{$(2, \bm{2}, 0)$ line slip} occurs due to finite-size effects
\end{itemize}
\end{tcolorbox}
\column{0.06\textwidth}
\centering
\includegraphics[width=\textwidth, clip=true, trim=25 -130 25 40]{Abbildungen/220lineslip.png}
\captionof{figure}{$(2, \bm{2}, 0)$ line~slip}
\column{0.25\textwidth}
\centering
\includegraphics[width=0.94\textwidth]{Abbildungen/SimulationPhaseDiagram.pdf}
\captionof{figure}{
Phase diagram of the finite-size simulations.
A homogeneous $(2, \bm{2}, 0)$ line slip occurs. 
}
\end{columns}

\end{block}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\vspace{-1cm}
%%%%%%%%%%%%%%%%%% REFERENCES %%%%%%%%%%%%%%%%%%%%%%%%
\vspace*{\fill}
\begin{columns}[b]
  \column{0.25\textwidth}
  \centering
  \includegraphics[height=6cm]{Abbildungen/sfi_logo.png}
  \column{0.5\textwidth}
  \centering
  \includegraphics[height=6cm, clip=true, trim=325 17 0 20]{Abbildungen/Longlogo.jpg}
  \column{0.25\textwidth}
  \begin{flushright}
  \includegraphics[height=6cm]{Abbildungen/qrcode.png}
   \hspace*{2cm}
  \end{flushright}
\end{columns}

\end{document}